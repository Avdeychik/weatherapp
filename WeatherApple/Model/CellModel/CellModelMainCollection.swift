//
//  CellModelMainCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

class CellModelMainCollection {
    
    let cellIdentifier: String
    
    init(cellIdentifier: String) {
        self.cellIdentifier = cellIdentifier
    }
}

