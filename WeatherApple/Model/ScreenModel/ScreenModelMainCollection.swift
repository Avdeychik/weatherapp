//
//  ScreenModelMainCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

protocol ScreenModelMainCollection: AnyObject {
    
    var cellModel: [CellModelMainCollection] { get }
}
