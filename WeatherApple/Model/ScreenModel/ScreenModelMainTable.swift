//
//  ScreenModelMainTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

protocol ScreenModelMainTable: AnyObject {
    
    var cellModel: [CellModelMainTable] { get }
}
