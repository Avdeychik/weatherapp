//
//  ScreenModelCustomCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

protocol ScreenModelCustomCollection: AnyObject {
    
    var cellModel: [CellModelCustomCollection] { get }
}
