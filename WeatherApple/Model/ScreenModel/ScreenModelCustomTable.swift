//
//  ScreenModelCustomTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

protocol ScreenModelCustomTable: AnyObject {

    var cellModel: [CellModelCustomTable] { get }
}
