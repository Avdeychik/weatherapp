//
//  WeatherModelCustomTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

class WeatherModelCustomTable: ScreenModelCustomTable {
    
    lazy var dataSource = WeatherDataSourceCustomTable(input: self)
    
    weak var output: CustomTableOutput?
    
    private(set) var cellModel: [CellModelCustomTable] = []

    init(output: CustomTableOutput) {
        self.output = output
    }
}

// MARK: - Network

extension WeatherModelCustomTable {
    
    func loadData() {
        NetworkController().fetchCurrentWeatherData(for: "Minsk") {
            [weak self] (model, error) in
            guard let self = self, let model = model else { return }
            self.cellModel = WeatherModelCustomTableFactory().cellModels(by: model)
            self.output?.reloadData()
        }
    }
}

// MARK: - Private

private extension WeatherModelCustomTable {

}

class WeatherModelCustomTableFactory {

    typealias Model = APIModels.CurrentWeather

    func cellModels(by model: Model) -> [CellModelCustomTable] {
        var result = [CellModelCustomTable]()
        result.append(mondayModel(by: model))
        result.append(tuesdayModel(by: model))
        result.append(wednesdayModel(by: model))
        result.append(thursdayModel(by: model))
        result.append(fridayModel(by: model))
        result.append(saturdayModel(by: model))
        result.append(sundayModel(by: model))
        result.append(pressureModel(by: model))
        result.append(windSpeedModel(by: model))
        result.append(sunriseModel(by: model))
        result.append(snowModel(by: model))
        return result
    }

    private func mondayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Понедельник",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func tuesdayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Вторник",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func wednesdayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Среда",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func thursdayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Четверг",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func fridayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Пятница",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func saturdayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Суббота",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func sundayModel(by model: Model) -> CellModelCustomTable {
        return WeatherDaysTableCellModel(
            day: "Воскресенье",
            weatherIcon: model.weather.first?.icon ?? "",
            minTemp: Int(model.main.temp_min),
            maxTemp: Int(model.main.temp_max)
            )
    }
    
    private func pressureModel(by model: Model) -> CellModelCustomTable {
        return PressureTableCellModel(
            textPressure: "Давление",
            pressure: Int(model.main.pressure),
            textHumidity: "Влажность",
            humidity: Double(model.main.humidity)
        )
    }
    
    private func windSpeedModel(by model: Model) -> CellModelCustomTable {
        return WindSpeedCellModel(
            textWindSpeed: "Ветер",
            windSpeed: Double(model.wind.speed),
            textFeelsLike: "Ощущается как",
            feelsLike: Double(model.main.feels_like)
        )
    }
    
    private func sunriseModel(by model: Model) -> CellModelCustomTable {
        func unixTimeConvertion(unixTime: Double) -> String {
            let time = NSDate(timeIntervalSince1970: unixTime)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            return dateFormatter.string(from: time as Date)
        }
        let sunriseStr = unixTimeConvertion(unixTime: Double(model.sys.sunrise))
        let sunsetStr = unixTimeConvertion(unixTime: Double(model.sys.sunset))
        return SunriseCellModel(
            sunriseText: "Восход солнца",
            sunrise: sunriseStr,
            sunsetText: "Заход солнца",
            sunet: sunsetStr
        )
    }
    
    private func snowModel(by model: Model) -> CellModelCustomTable {
        return VisibilityCellModel(
            visibilityText: "Видимость",
            visibility: Int(model.visibility!),
            idCityText: "ID Страны",
            idCity: "\(model.sys.country)"
        )
    }
}
