//
//  WeatherModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

class WeatherModel: ScreenModelMainCollection {
    
    lazy var dataSource = WeatherDataSourceCollection(input: self)
    
    private(set) var cellModel: [CellModelMainCollection] = [
        WeatherMainScreenCollectionCellModel(),
        WeatherMainScreenCollectionCellModel(),
        WeatherMainScreenCollectionCellModel()
    ]
}
