//
//  WeatherTableModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

class WeatherTableModel: ScreenModelMainTable {
    
    lazy var dataSource = WeatherDataSourceTable(input: self)
    
    weak var output: WeatherMainScreenCollectionOutput?
    
    private(set) var cellModel: [CellModelMainTable] = []
    
    init(output: WeatherMainScreenCollectionOutput) {
        self.output = output
    }
}

// MARK: - Network

extension WeatherTableModel {
    
    func loadData() {
        NetworkController().fetchCurrentWeatherData(for: "Minsk") {
            [weak self] (model, error) in
            guard let self = self, let model = model else { return }
            self.cellModel = WeatherModelMainFactory().cellModels(by: model)
            self.output?.reloadData()
        }
    }
}

// MARK: - Private

class WeatherModelMainFactory {
    
    typealias Model = APIModels.CurrentWeather
    
    func cellModels(by model: Model) -> [CellModelMainTable] {
        var result = [CellModelMainTable]()
        result.append(generalWeatherCellModel(by: model))
        result.append(customHorizontalCollectionCellModel())
        result.append(customTableCellModel())
        return result
    }
    
    private func generalWeatherCellModel(by model: Model) -> CellModelMainTable {
        return GeneralWeatherTableCellModel(
            textCity: model.name,
            textWeather: model.weather.first?.description ?? "",
            textdegree: Int(model.main.temp)
        )
    }
    
    private func customHorizontalCollectionCellModel() -> CellModelMainTable {
        return CustomHorizontalCollectionCellModel()
    }
    
    private func customTableCellModel() -> CellModelMainTable {
        return CustomTableCellModel()
    }
}
