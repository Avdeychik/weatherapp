//
//  WeatherModelCustomCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

class WeatherModelCustomCollection: ScreenModelCustomCollection {
    
    lazy var dataSource = WeatherDataSourceCustomCollection(input: self)
    
    weak var output: CustomHorizontalCollectionOutput?
    
    private(set) var cellModel: [CellModelCustomCollection] = []
    
    init(output: CustomHorizontalCollectionOutput) {
        self.output = output
    }
}

// MARK: - Network

extension WeatherModelCustomCollection {
    
    func loadData() {
        NetworkController().fetchCurrentWeatherData(for: "Minsk") {
            [weak self] (model, error) in
            guard let self = self, let model = model else { return }
            self.cellModel = WeatherModelCustomCollectionFactory().cellModels(by: model)
            self.output?.reloadData()
        }
    }
}

// MARK: - Private

private extension WeatherTableModel {
    
}

class WeatherModelCustomCollectionFactory {
    
    typealias Model = APIModels.CurrentWeather
    
    func cellModels(by model: Model) -> [CellModelCustomCollection] {
        var result = [CellModelCustomCollection]()
        result.append(hourlyWeatherCellModel11(by: model))
        result.append(hourlyWeatherCellModel12(by: model))
        result.append(hourlyWeatherCellModel13(by: model))
        result.append(hourlyWeatherCellModel14(by: model))
        result.append(hourlyWeatherCellModel15(by: model))
        result.append(hourlyWeatherCellModel16(by: model))
        result.append(hourlyWeatherCellModel17(by: model))
        result.append(hourlyWeatherCellModel18(by: model))
        result.append(hourlyWeatherCellModel19(by: model))
        result.append(hourlyWeatherCellModel20(by: model))
        result.append(hourlyWeatherCellModel21(by: model))
        result.append(hourlyWeatherCellModel22(by: model))
        result.append(hourlyWeatherCellModel23(by: model))
        result.append(hourlyWeatherCellModel0(by: model))
        result.append(hourlyWeatherCellModel1(by: model))
        result.append(hourlyWeatherCellModel2(by: model))
        result.append(hourlyWeatherCellModel3(by: model))
        result.append(hourlyWeatherCellModel4(by: model))
        result.append(hourlyWeatherCellModel5(by: model))
        result.append(hourlyWeatherCellModel6(by: model))
        

        return result
    }
    
    private func hourlyWeatherCellModel11(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "11",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel12(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "12",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel13(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "13",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel14(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "14",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel15(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "15",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel16(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "16",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel17(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "17",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel18(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "18",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel19(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "19",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel20(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "20",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel21(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "21",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel22(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "22",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel23(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "23",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel0(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "0",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel1(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "1",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel2(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "2",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel3(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "3",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel4(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "4",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel5(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "5",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel6(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "6",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel7(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "7",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel8(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "8",
            degree: Int(model.main.temp_max),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel9(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "9",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
    
    private func hourlyWeatherCellModel10(by model: Model) -> CellModelCustomCollection {
        return WeatherHoursCollectionCellModel(
            time: "10",
            degree: Int(model.main.temp_min),
            iconType: model.weather.first?.icon ?? ""
        )
    }
}
