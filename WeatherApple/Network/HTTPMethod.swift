//
//  HTTPMethod.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 21.12.21.
//

import Foundation

public enum HTTPMethod: String {
    case delete = "DELETE"
    case get = "GET"
    case patch = "PATCH"
    case post = "POST"
    case put = "PUT"
}
