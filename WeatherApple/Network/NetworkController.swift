//
//  NetworkController.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 21.12.21.
//

import Foundation

class NetworkController {
    let host = "https://api.openweathermap.org/"
    let apiService = APIService()
    
    func fetchCurrentWeatherData(
        for cityName: String,
        completion: @escaping (APIModels.CurrentWeather?, Error?) -> Void
    ) {
        let parameters = [
            URLQueryItem(name: "q", value: cityName),
        ]
        apiService.loadData(
            host: host,
            endpoint: Endpoint.currentWeather.rawValue,
            method: .get,
            parameters: parameters
        ) { data, error in

            if let error = error {
                completion(nil, error)
                return
            }

            guard let data = data else {
                completion(nil, error)
                return
            }

            let model = try? JSONDecoder().decode(APIModels.CurrentWeather.self, from: data)
            completion(model, error)
        }
    }
    
    func fetchDailyWeather(comlpetion: @escaping (DailyWeatherModel?, Error?) -> Void){
        
        let host = "https://api.openweathermap.org/"
        let apiKey = "38b7dbdb395a9e3b05f86def8f5205d8"
        let lat = "53.902284"
        let lon = "27.561831"
        let exclude = "daily"
        
        let urlString = "\(host)data/2.5/onecall?lat=\(lat)&lon=\(lon)&exclude=\(exclude)&appid=\(apiKey)"
        
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            if let error = error {
                print(error)
                return
            }
            
            guard let data = data else { return }
            let model = try? JSONDecoder().decode(DailyWeatherModel.self, from: data)
            comlpetion(model, error)
        }
        
        task.resume()
    }
}
