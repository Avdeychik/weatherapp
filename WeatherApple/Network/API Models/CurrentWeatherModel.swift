//
//  CurrentWeatherModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 21.12.21.
//

import Foundation

// MARK: - CurrentWeather

extension APIModels {
    
    struct CurrentWeather: Decodable {
        let id: Int
        let cod: Int
        let name: String
        let dt: Int?
        let base: String?
        let visibility: Int?
        
        let main: Main
        let wind: Wind
        let weather: [Weather]
        let sys: Sys
    }
}

// MARK: - Weather

extension APIModels {
    
    struct Weather: Decodable {
        let id: Int
        let main: String
        let description: String?
        let icon: String?
    }
}

// MARK: - Main

extension APIModels {
    
    struct Main: Decodable {
        let temp: Double
        let feels_like: Double
        let pressure: Int
        let humidity: Int
        let temp_min: Double
        let temp_max: Double
    }
}

// MARK: - Wind

extension APIModels {
    
    struct Wind: Decodable {
        let speed: Double
        let deg: Int
    }
}

extension APIModels {
    
    struct Sys: Decodable {
        let type: Int
        let id: Int
        let country: String
        let sunrise: Int
        let sunset: Int 
    }
}


