//
//  Endpoint.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 21.12.21.
//

import Foundation

enum Endpoint: String {
    case currentWeather = "data/2.5/weather"
}
