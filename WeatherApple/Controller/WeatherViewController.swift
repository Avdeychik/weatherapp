//
//  WeatherViewController.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 15.12.21.
//

import UIKit

class WeatherViewController: UIViewController {
    
    private let gradientlayer = CAGradientLayer()
    
    private let model: WeatherModel
    
    init(model: WeatherModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
    //MARK: UI
    
    private let searchButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "list-icon"), for: .normal)
        return button
    }()
    
    private let pageControl: UIPageControl = {
        let pageControl = UIPageControl(frame: CGRect(x: 100, y: 100, width: 100, height: 40))
        pageControl.numberOfPages = 2
        return pageControl
    }()
    
    lazy var weatherCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.addSublayer(gradientlayer)
        weatherCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        weatherCollectionView.delegate = self
        weatherCollectionView.dataSource = model.dataSource
        weatherCollectionView.showsHorizontalScrollIndicator = false
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setBlueGradientBackground()
    }
    
    //MARK: Setup UI Elements
    
    private func setupViews() {
        configureWeatherCollectionView()
        configurePageControl()
        configureSearchButton()
    }
    
    func configureWeatherCollectionView() {
        view.addSubview(weatherCollectionView)
        weatherCollectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            weatherCollectionView.topAnchor.constraint(equalTo: view.topAnchor),
            weatherCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            weatherCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            weatherCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40)
        ])
    }
    
    func configurePageControl() {
        view.addSubview(pageControl)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            pageControl.leftAnchor.constraint(equalTo: view.leftAnchor),
            pageControl.rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
    }
    
    func configureSearchButton () {
        pageControl.addSubview(searchButton)
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchButton.topAnchor.constraint(equalTo: pageControl.topAnchor),
            searchButton.rightAnchor.constraint(equalTo: pageControl.rightAnchor, constant: -40),
            searchButton.bottomAnchor.constraint(equalTo: pageControl.bottomAnchor, constant: -10),
            searchButton.widthAnchor.constraint(equalToConstant: 20),
            searchButton.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
}

//MARK: extension CollectionDelegateFlowLayout

extension WeatherViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return .init(width: width, height: height)
    }
}

//MARK: - Gradient Background
extension WeatherViewController {
    
    func setBlueGradientBackground() {
        let topColor = UIColor(red: 195.0/255.0, green: 165.0/255.0, blue: 1.0, alpha: 1.0).cgColor
        let midleColor = UIColor(red: 172.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        let bottomColor = UIColor(red: 12.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        let top1 = UIColor(red: 12.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        let top2 = UIColor(red: 12.0/255.0, green: 154.0/255.0, blue: 214.0/255.0, alpha: 1.0).cgColor
        let top3 = UIColor(red: 112.0/255.0, green: 114.0/255.0, blue: 184.0/255.0, alpha: 1.0).cgColor
        gradientlayer.frame = view.bounds
        gradientlayer.colors = [topColor, midleColor, bottomColor, top1, top2, top3]
    }
}

