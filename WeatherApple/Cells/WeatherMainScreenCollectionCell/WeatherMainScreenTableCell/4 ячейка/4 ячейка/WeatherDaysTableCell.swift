//
//  WeatherDaysTableCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import UIKit

class WeatherDaysTableCell: UITableViewCell {
    
    static let cellIdentifier = "WeatherDaysTableCell"
    
    weak var cellModel: WeatherDaysTableCellModel?
    
    let dayNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .white
        label.backgroundColor = .clear

        return label
    }()
    
    let weatherIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let temperatureMinLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .white
        return label
    }()
    
    let temperatureMaxLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .white
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
        selectionStyle = .none

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configureDayNameLabel()
        configureWeatherIconImageView()
        configureTemperatureMaxLabel()
        configureTemperatureMinLabel()
    }

    func configureDayNameLabel() {
        contentView.addSubview(dayNameLabel)
        dayNameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dayNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            dayNameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 25),
            dayNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            dayNameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    
    func configureWeatherIconImageView() {
        contentView.addSubview(weatherIconImageView)
        weatherIconImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            weatherIconImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            weatherIconImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            weatherIconImageView.heightAnchor.constraint(equalToConstant: 40),
            weatherIconImageView.widthAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func configureTemperatureMaxLabel() {
        contentView.addSubview(temperatureMaxLabel)
        temperatureMaxLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            temperatureMaxLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            temperatureMaxLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            temperatureMaxLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -85),
            temperatureMaxLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5)
        ])
    }
    
    func configureTemperatureMinLabel() {
        contentView.addSubview(temperatureMinLabel)
        temperatureMinLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            temperatureMinLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            temperatureMinLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -25),
            temperatureMinLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            temperatureMinLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
}

extension WeatherDaysTableCell: FillableCellCustomTable {
    
    func fill(cellModel: CellModelCustomTable) {
        guard let cellModel = cellModel as? WeatherDaysTableCellModel else { return }
        self.cellModel = cellModel
        self.dayNameLabel.text = cellModel.day
        self.weatherIconImageView.image = UIImage(named: cellModel.weatherIcon)
        self.temperatureMaxLabel.text = cellModel.maxTempString
        self.temperatureMinLabel.text = cellModel.minTempString
    }
}
