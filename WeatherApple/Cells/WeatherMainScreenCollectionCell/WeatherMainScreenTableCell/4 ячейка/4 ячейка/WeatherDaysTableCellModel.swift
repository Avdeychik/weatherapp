//
//  WeatherDaysTableCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

class WeatherDaysTableCellModel: CellModelCustomTable {
    let day: String
    let weatherIcon: String
    let minTemp: Int
    let maxTemp: Int
    
    init(day: String, weatherIcon: String, minTemp: Int, maxTemp: Int) {
        self.day = day
        self.weatherIcon = weatherIcon
        self.minTemp = minTemp
        self.maxTemp = maxTemp
        super.init(cellIdentifier: WeatherDaysTableCell.cellIdentifier)
    }
}

extension WeatherDaysTableCellModel {
    var minTempString: String {
        return "\(minTemp) º"
    }
    
    var maxTempString: String {
        return "\(maxTemp) º"
    }
}
