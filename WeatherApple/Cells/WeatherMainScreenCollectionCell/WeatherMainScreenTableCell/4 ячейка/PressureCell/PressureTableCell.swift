//
//  PressureTableCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 21.12.21.
//

import UIKit

class PressureTableCell: UITableViewCell {
    
    static let cellIdentifier = "PressureTableCell"
    
    weak var cellModel: PressureTableCellModel?
    
    let pressureLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let pressure: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let humidityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let humidity: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let pressureStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    let humidityStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configurePressureStackView()
        configureHumidityStackView()
        configurePessureLabel()
        configurePessure()
        configureHumidityLabel()
        configureHumidity()
    }
    
    func configurePressureStackView() {
        contentView.addSubview(pressureStackView)
        pressureStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pressureStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            pressureStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            pressureStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            pressureStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -130),
        ])
    }
    
    func configureHumidityStackView() {
        contentView.addSubview(humidityStackView)
        humidityStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            humidityStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            humidityStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            humidityStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            humidityStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 130),
            
        ])
    }
    
    func configurePessureLabel() {
        contentView.addSubview(pressureLabel)
        pressureStackView.addArrangedSubview(pressureLabel)
    }
    
    func configurePessure() {
        contentView.addSubview(pressure)
        pressureStackView.addArrangedSubview(pressure)
    }
    
    func configureHumidityLabel() {
        contentView.addSubview(humidityLabel)
        humidityStackView.addArrangedSubview(humidityLabel)
    }
    
    func configureHumidity() {
        contentView.addSubview(humidity)
        humidityStackView.addArrangedSubview(humidity)
    }
}

extension PressureTableCell: FillableCellCustomTable {
    
    func fill(cellModel: CellModelCustomTable) {
        guard let cellModel = cellModel as? PressureTableCellModel else { return }
        self.cellModel = cellModel
        pressureLabel.text = cellModel.textPressure
        pressure.text = cellModel.pressureString
        humidityLabel.text = cellModel.textHumidity
        humidity.text = cellModel.humidityString
    }
}
