//
//  PressureTableCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 21.12.21.
//

import Foundation

class PressureTableCellModel: CellModelCustomTable {

    let textPressure: String
    let pressure: Int
    let textHumidity: String
    let humidity: Double
    
    init(textPressure: String, pressure: Int, textHumidity: String, humidity: Double) {
        self.textPressure = textPressure
        self.pressure = pressure
        self.textHumidity = textHumidity
        self.humidity = humidity
        super.init(cellIdentifier: PressureTableCell.cellIdentifier)
    }
}

extension PressureTableCellModel {
    var pressureString: String {
        return "\(pressure) гПа"
    }
    
    var humidityString: String {
        return "\(humidity) %"
    }
}
