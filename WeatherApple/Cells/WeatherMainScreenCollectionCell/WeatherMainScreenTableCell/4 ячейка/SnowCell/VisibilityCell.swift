//
//  VisibilityCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 23.12.21.
//

import UIKit

class VisibilityCell: UITableViewCell {
    
    static let cellIdentifier = "VisibilityCell"
    
    weak var cellModel: VisibilityCellModel?
    
    let visibilityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let visibility: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let idCityLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let idCity: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let snowStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    let idStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configureSunsiseStackView()
        configureSunsetStackView()
        configureSunriseLabel()
        configureSunrise()
        configureSunsetLabel()
        configureSunset()
    }
    
    func configureSunsiseStackView() {
        contentView.addSubview(snowStackView)
        snowStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            snowStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            snowStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            snowStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            snowStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -130),
        ])
    }
    
    func configureSunsetStackView() {
        contentView.addSubview(idStackView)
        idStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            idStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            idStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            idStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            idStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 130),
        ])
    }
    
    func configureSunriseLabel() {
        contentView.addSubview(visibilityLabel)
        snowStackView.addArrangedSubview(visibilityLabel)
    }
    
    func configureSunrise() {
        contentView.addSubview(visibility)
        snowStackView.addArrangedSubview(visibility)
    }
    
    func configureSunsetLabel() {
        contentView.addSubview(idCityLabel)
        idStackView.addArrangedSubview(idCityLabel)
    }
    
    func configureSunset() {
        contentView.addSubview(idCity)
        idStackView.addArrangedSubview(idCity)
    }
}

extension VisibilityCell: FillableCellCustomTable {
    
    func fill(cellModel: CellModelCustomTable) {
        guard let cellModel = cellModel as? VisibilityCellModel else { return }
        visibilityLabel.text = cellModel.visibilityText
        visibility.text = "\(cellModel.visibility / 1000) км"
        idCityLabel.text = cellModel.idCityText
        idCity.text = cellModel.idCity
    }
    
    
}
