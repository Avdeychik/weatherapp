//
//  VisibilityCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 23.12.21.
//

import Foundation

class VisibilityCellModel: CellModelCustomTable {
    
    let visibilityText: String
    let visibility: Int
    let idCityText: String
    let idCity: String
    
    init(visibilityText: String, visibility: Int, idCityText: String, idCity: String) {
        self.visibilityText = visibilityText
        self.visibility = visibility
        self.idCityText = idCityText
        self.idCity = idCity
        super.init(cellIdentifier: VisibilityCell.cellIdentifier)
    }
}
