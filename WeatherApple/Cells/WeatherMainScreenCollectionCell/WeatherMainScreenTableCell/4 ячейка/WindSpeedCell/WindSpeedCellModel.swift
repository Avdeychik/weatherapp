//
//  WindSpeedCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 22.12.21.
//

import Foundation

class WindSpeedCellModel: CellModelCustomTable {

    let textWindSpeed: String
    let windSpeed: Double
    let textFeelsLike: String
    let feelsLike: Double
    
    init(textWindSpeed: String, windSpeed: Double, textFeelsLike: String, feelsLike: Double) {
        self.textWindSpeed = textWindSpeed
        self.windSpeed = windSpeed
        self.textFeelsLike = textFeelsLike
        self.feelsLike = feelsLike
        super.init(cellIdentifier: WindSpeedCell.cellIdentifier)
    }
}

extension WindSpeedCellModel {
    var windSpeedString: String {
        return "\(windSpeed) м/с"
    }
    
    var feelsLikeString: String {
        return "\(feelsLike) º"
    }
}
