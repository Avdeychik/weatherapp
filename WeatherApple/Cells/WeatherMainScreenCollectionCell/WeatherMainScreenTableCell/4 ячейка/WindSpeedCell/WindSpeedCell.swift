//
//  WindSpeedCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 22.12.21.
//

import UIKit

class WindSpeedCell: UITableViewCell {
    
    static let cellIdentifier = "WindSpeedCell"
    
    weak var cellModel: WindSpeedCellModel?
    
    let windSpeedLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let windSpeed: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let feelsLikeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let feelsLike: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let windSpeedStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    let windDirectionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configureWindSpeedStackView()
        configurefeelsLikeStackView()
        configureWindSpeedLabel()
        configureWindSpeed()
        configurefeelsLikeLabel()
        configurefeelsLike()
    }
    
    func configureWindSpeedStackView() {
        contentView.addSubview(windSpeedStackView)
        windSpeedStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            windSpeedStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            windSpeedStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            windSpeedStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            windSpeedStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -130),
        ])
    }
    
    func configurefeelsLikeStackView() {
        contentView.addSubview(windDirectionStackView)
        windDirectionStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            windDirectionStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            windDirectionStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            windDirectionStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            windDirectionStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 130),
        ])
    }
    
    func configureWindSpeedLabel() {
        contentView.addSubview(windSpeedLabel)
        windSpeedStackView.addArrangedSubview(windSpeedLabel)
    }
    
    func configureWindSpeed() {
        contentView.addSubview(windSpeed)
        windSpeedStackView.addArrangedSubview(windSpeed)
    }
    
    func configurefeelsLikeLabel() {
        contentView.addSubview(feelsLikeLabel)
        windDirectionStackView.addArrangedSubview(feelsLikeLabel)
    }
    
    func configurefeelsLike() {
        contentView.addSubview(feelsLike)
        windDirectionStackView.addArrangedSubview(feelsLike)
    }
}

extension WindSpeedCell: FillableCellCustomTable {
    
    func fill(cellModel: CellModelCustomTable) {
        guard let cellModel = cellModel as? WindSpeedCellModel else { return }
        self.cellModel = cellModel
        windSpeedLabel.text = cellModel.textWindSpeed
        windSpeed.text = cellModel.windSpeedString
        feelsLikeLabel.text = cellModel.textFeelsLike
        feelsLike.text = cellModel.feelsLikeString
    }
}

