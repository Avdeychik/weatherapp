//
//  SunriseCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 22.12.21.
//

import UIKit

class SunriseCell: UITableViewCell {
    
    static let cellIdentifier = "SunriseCell"
    
    weak var cellModel: SunriseCellModel?
    
    let sunriseLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let sunrise: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let sunsetLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let sunset: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let sunriseStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    let sunsetStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configureSunsiseStackView()
        configureSunsetStackView()
        configureSunriseLabel()
        configureSunrise()
        configureSunsetLabel()
        configureSunset()
    }
    
    func configureSunsiseStackView() {
        contentView.addSubview(sunriseStackView)
        sunriseStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sunriseStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            sunriseStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            sunriseStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            sunriseStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -130),
        ])
    }
    
    func configureSunsetStackView() {
        contentView.addSubview(sunsetStackView)
        sunsetStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sunsetStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            sunsetStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            sunsetStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            sunsetStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 130),
        ])
    }
    
    func configureSunriseLabel() {
        contentView.addSubview(sunriseLabel)
        sunriseStackView.addArrangedSubview(sunriseLabel)
    }
    
    func configureSunrise() {
        contentView.addSubview(sunrise)
        sunriseStackView.addArrangedSubview(sunrise)
    }
    
    func configureSunsetLabel() {
        contentView.addSubview(sunsetLabel)
        sunsetStackView.addArrangedSubview(sunsetLabel)
    }
    
    func configureSunset() {
        contentView.addSubview(sunset)
        sunsetStackView.addArrangedSubview(sunset)
    }
}

extension SunriseCell: FillableCellCustomTable {
    
    func fill(cellModel: CellModelCustomTable) {
        guard let cellModel = cellModel as? SunriseCellModel else { return }
        self.cellModel = cellModel
        sunriseLabel.text = cellModel.sunriseText
        sunrise.text = cellModel.sunrise
        sunsetLabel.text = cellModel.sunsetText
        sunset.text = cellModel.sunet
    }
}

