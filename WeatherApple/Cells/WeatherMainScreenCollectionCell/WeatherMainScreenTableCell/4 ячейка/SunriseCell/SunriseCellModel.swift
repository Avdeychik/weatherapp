//
//  SunriseCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 22.12.21.
//

import Foundation

class SunriseCellModel: CellModelCustomTable {
    
    let sunriseText: String
    let sunrise: String
    let sunsetText: String
    let sunet: String
    
    init(sunriseText: String, sunrise: String, sunsetText: String, sunet: String) {
        self.sunriseText = sunriseText
        self.sunrise = sunrise
        self.sunsetText = sunsetText
        self.sunet = sunet
        super.init(cellIdentifier: SunriseCell.cellIdentifier)
    }
}
