//
//  WeatherDaysTableCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import UIKit

protocol CustomTableOutput: AnyObject {
    func reloadData()
}

class CustomTableCell: UITableViewCell {
    
    static let cellIdentifier = "CustomTableCell"
    
    weak var cellModel: CustomTableCellModel?
    
    lazy var model = WeatherModelCustomTable(output: self)
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureView()
        tableView.dataSource = model.dataSource
        tableView.showsVerticalScrollIndicator = false
        model.loadData()
        tableView.separatorStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configureTableView()
    }
    
    func configureTableView() {
        contentView.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tableView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            tableView.heightAnchor.constraint(equalToConstant: 300)
        ])
    }
}

extension CustomTableCell: FillableCellTable {
    
    func fill(cellModel: CellModelMainTable) {
        guard let cellModel = cellModel as? CustomTableCellModel else { return }
        self.cellModel = cellModel
    }
}

extension CustomTableCell: CustomTableOutput {
    
    func reloadData() {
        tableView.reloadData()
    }
}
