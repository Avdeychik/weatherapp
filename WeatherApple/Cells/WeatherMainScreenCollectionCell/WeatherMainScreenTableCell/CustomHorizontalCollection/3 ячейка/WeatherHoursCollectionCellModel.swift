//
//  WeatherHoursCollectionCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

class WeatherHoursCollectionCellModel: CellModelCustomCollection {
    
    let time: String
    let degree: Int
    let iconType: String
    
    init(time: String, degree: Int, iconType: String) {
        self.time = time
        self.degree = degree
        self.iconType = iconType
        super.init(cellIdentifier: WeatherHoursCollectionCell.cellIdentifier)
    }
}

extension WeatherHoursCollectionCellModel {
    var degreeString: String {
        return "\(degree) º"
    }
}
