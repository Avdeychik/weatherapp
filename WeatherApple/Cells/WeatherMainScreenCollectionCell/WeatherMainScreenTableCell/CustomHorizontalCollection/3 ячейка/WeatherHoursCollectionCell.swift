//
//  WeatherHoursCollectionCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import UIKit

class WeatherHoursCollectionCell: UICollectionViewCell {
    
    
    static let cellIdentifier = "WeatherHoursCollectionCell"
    
    weak var cellModel: WeatherHoursCollectionCellModel?
    
    //MARK: UI
    
    let weatherStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    let weatherIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let degreeWeatherLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = .white
        label.backgroundColor = .clear
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .clear
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configure views
    
    private func configureView() {
        configureStackView()
        configurelabel1()
        configurelabel2()
        configurelabel3()
    }
    
    func configureStackView() {
        contentView.addSubview(weatherStack)
        weatherStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            weatherStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            weatherStack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            weatherStack.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            weatherStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
        ])
    }
    
    func configurelabel1() {
        contentView.addSubview(timeLabel)
        weatherStack.addArrangedSubview(timeLabel)
        NSLayoutConstraint.activate([
            timeLabel.widthAnchor.constraint(equalToConstant: 30),
            timeLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    func configurelabel2() {
        contentView.addSubview(weatherIconImageView)
        weatherStack.addArrangedSubview(weatherIconImageView)
        NSLayoutConstraint.activate([
            weatherIconImageView.widthAnchor.constraint(equalToConstant: 30),
            weatherIconImageView.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    func configurelabel3() {
        contentView.addSubview(degreeWeatherLabel)
        weatherStack.addArrangedSubview(degreeWeatherLabel)
        NSLayoutConstraint.activate([
            degreeWeatherLabel.widthAnchor.constraint(equalToConstant: 30),
            degreeWeatherLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
}

// MARK: - FillableCell

extension WeatherHoursCollectionCell: FillableCellCustomCollection {
    
    func fill(cellModel: CellModelCustomCollection) {
        guard let cellModel = cellModel as? WeatherHoursCollectionCellModel else { return }
        self.cellModel = cellModel
        self.timeLabel.text = cellModel.time
        self.weatherIconImageView.image = UIImage(named: cellModel.iconType)
        self.degreeWeatherLabel.text = cellModel.degreeString
    }
}
