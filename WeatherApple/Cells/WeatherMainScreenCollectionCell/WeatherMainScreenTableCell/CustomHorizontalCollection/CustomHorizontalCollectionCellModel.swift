//
//  CustomHorizontalCollectionCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

class CustomHorizontalCollectionCellModel: CellModelMainTable {
    
    init() {
        super.init(cellIdentifier: CustomHorizontalCollectionCell.cellIdentifier)
    }
}
