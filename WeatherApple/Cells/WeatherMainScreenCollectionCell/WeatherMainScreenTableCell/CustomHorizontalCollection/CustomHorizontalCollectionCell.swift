//
//  CustomHorizontalCollectionCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

protocol CustomHorizontalCollectionOutput: AnyObject {
    func reloadData()
}

class CustomHorizontalCollectionCell: UITableViewCell {
    
    static let cellIdentifier = "CustomHorizontalCollectionCell"
    
    weak var cellModel: CustomHorizontalCollectionCellModel?
    
    lazy var model = WeatherModelCustomCollection(output: self)
    
    lazy var weatherCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        layout.scrollDirection = .horizontal
        collectionView.backgroundColor = .clear
        return collectionView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        configureView()
        weatherCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        weatherCollectionView.delegate = self
        weatherCollectionView.dataSource = model.dataSource
        weatherCollectionView.showsHorizontalScrollIndicator = false
        model.loadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        contentView.addSubview(weatherCollectionView)
        weatherCollectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            weatherCollectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            weatherCollectionView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            weatherCollectionView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            weatherCollectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            weatherCollectionView.heightAnchor.constraint(equalToConstant: 130)
        ])
    }
}
    
extension CustomHorizontalCollectionCell: FillableCellTable {
    
    func fill(cellModel: CellModelMainTable) {
        guard let cellModel = cellModel as? CustomHorizontalCollectionCellModel else { return }
        self.cellModel = cellModel
    }
}

extension CustomHorizontalCollectionCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / 5
        let height = collectionView.bounds.height
        return .init(width: width, height: height)
    }
}

extension CustomHorizontalCollectionCell: CustomHorizontalCollectionOutput {
    
    func reloadData() {
        weatherCollectionView.reloadData()
    }
}
