//
//  GeneralWeatherTableCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

class GeneralWeatherTableCell: UITableViewCell {
    
    static let cellIdentifier = "GeneralWeatherTableCell"
    
    weak var cellModel: GeneralWeatherTableCellModel?
    
    //MARK: UI
    
    let cityLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 33)
        label.textColor = .white
        return label
    }()
    
    let textWeatherLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 23)
        label.textColor = .white
        return label
    }()
    
    let degreeWeatherLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 63)
        label.textColor = .white
        return label
    }()
    
    let containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    //MARK: Inits
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .clear
        configureView()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Configure views
    
    private func configureView() {
        configureContainerStackView()
        configureCityLabel()
        configureTextWeatherLabel()
        configureDegreeWeatherLabel()
    }
    
    func configureContainerStackView() {
        contentView.addSubview(containerStackView)
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            containerStackView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            containerStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
            containerStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
        ])
    }
    
    func configureCityLabel() {
        contentView.addSubview(cityLabel)
        containerStackView.addArrangedSubview(cityLabel)
    }
    
    func configureTextWeatherLabel() {
        contentView.addSubview(textWeatherLabel)
        containerStackView.addArrangedSubview(textWeatherLabel)
    }
    
    func configureDegreeWeatherLabel() {
        contentView.addSubview(degreeWeatherLabel)
        containerStackView.addArrangedSubview(degreeWeatherLabel)
    }
}

extension GeneralWeatherTableCell: FillableCellTable {
    
    func fill(cellModel: CellModelMainTable) {
        guard let cellModel = cellModel as? GeneralWeatherTableCellModel else { return }
        self.cellModel = cellModel
        self.cityLabel.text = cellModel.textCity
        self.textWeatherLabel.text = cellModel.textWeather
        self.degreeWeatherLabel.text = cellModel.textdegreeString
        
    }
}
