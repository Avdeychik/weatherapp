//
//  GeneralWeatherTableCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

class GeneralWeatherTableCellModel: CellModelMainTable {
    
    let textCity: String
    let textWeather: String
    let textdegree: Int
    
    init(textCity: String, textWeather: String, textdegree: Int) {
        self.textCity = textCity
        self.textWeather = textWeather
        self.textdegree = textdegree
        super.init(cellIdentifier: GeneralWeatherTableCell.cellIdentifier)
    }
}

extension GeneralWeatherTableCellModel {
    var textdegreeString: String {
        return "\(textdegree) º"
    }
}
