//
//  WeatherMainScreenCollectionCell.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

protocol WeatherMainScreenCollectionOutput: AnyObject {
    func reloadData()
}

class WeatherMainScreenCollectionCell: UICollectionViewCell {
    
    static let cellIdentifier = "WeatherMainScreenCollectionCell"
    
    weak var cellModel: WeatherMainScreenCollectionCellModel?
    
    lazy var model = WeatherTableModel(output: self)
    
    let mainViewHeader: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 200))
        view.backgroundColor = .clear
        return view
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(mainViewHeader)
        tableView.dataSource = model.dataSource
        tableView.tableHeaderView = mainViewHeader
        tableView.showsVerticalScrollIndicator = false
        configureView()
        model.loadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureView() {
        configureTableView()
    }
    
    func configureTableView() {
        contentView.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tableView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
}

extension WeatherMainScreenCollectionCell: FillableCellCollection {
    
    func fill(cellModel: CellModelMainCollection) {
        guard let cellModel = cellModel as? WeatherMainScreenCollectionCellModel else { return }
        self.cellModel = cellModel
    }
}

extension WeatherMainScreenCollectionCell: WeatherMainScreenCollectionOutput {
    
    func reloadData() {
        tableView.reloadData()
    }
}
