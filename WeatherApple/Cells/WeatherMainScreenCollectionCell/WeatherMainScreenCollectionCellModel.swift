//
//  WeatherMainScreenCollectionCellModel.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

class WeatherMainScreenCollectionCellModel: CellModelMainCollection {

    init() {
        super.init(cellIdentifier: WeatherMainScreenCollectionCell.cellIdentifier)
    }
}
