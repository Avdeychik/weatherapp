//
//  FillableCellCustomTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

protocol FillableCellCustomTable {
    
    func fill(cellModel: CellModelCustomTable)
}
