//
//  FillableCellCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

protocol FillableCellCollection {
    
    func fill(cellModel: CellModelMainCollection)
}
