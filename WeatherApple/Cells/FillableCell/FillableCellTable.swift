//
//  FillableCellTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

protocol FillableCellTable {
    
    func fill(cellModel: CellModelMainTable)
}
