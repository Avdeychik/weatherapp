//
//  WeatherDataSourceTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

class WeatherDataSourceTable: NSObject, DataSourceMainTable, UITableViewDataSource {
    
    unowned var input: ScreenModelMainTable
    
    required init(input: ScreenModelMainTable) {
        self.input = input
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return input.cellModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard input.cellModel.count > indexPath.row else { return .init() }
        let cellModel = input.cellModel[indexPath.row]
        tableView.register(typeInString: cellModel.cellIdentifier)
        let cell = tableView.dequeueReusableCell(
            withIdentifier: cellModel.cellIdentifier,
            for: indexPath
        )
        
        cell.backgroundColor = .clear
        if let fillableCell = cell as? FillableCellTable {
            fillableCell.fill(cellModel: cellModel)
        }
        
        return cell
    }
}
