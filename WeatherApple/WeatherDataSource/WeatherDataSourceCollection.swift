//
//  WeatherDataSourceCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

class WeatherDataSourceCollection: NSObject, DataSourceMainCollection, UICollectionViewDataSource {
    
    unowned var input: ScreenModelMainCollection
    
    init(input: ScreenModelMainCollection) {
        self.input = input
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return input.cellModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard input.cellModel.count > indexPath.row else { return .init() }
        let cellModel = input.cellModel[indexPath.row]
        collectionView.register(typeInString: cellModel.cellIdentifier)
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellModel.cellIdentifier, for: indexPath
        )
        cell.backgroundColor = .clear
        
        if let fillableCell = cell as? FillableCellCollection {
            fillableCell.fill(cellModel: cellModel)
        }
        
        return cell
    }
}
