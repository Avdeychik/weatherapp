//
//  WeatherDataSourceCustomTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import UIKit

class WeatherDataSourceCustomTable: NSObject, DataSourceCustomTable, UITableViewDataSource {
    
    unowned var input: ScreenModelCustomTable
    
    required init(input: ScreenModelCustomTable) {
        self.input = input
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return input.cellModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard input.cellModel.count > indexPath.row else { return .init() }
        let cellModel = input.cellModel[indexPath.row]
        tableView.register(typeInString: cellModel.cellIdentifier)
        let cell = tableView.dequeueReusableCell(
            withIdentifier: cellModel.cellIdentifier,
            for: indexPath
        )
        cell.backgroundColor = .clear
        if let fillableCell = cell as? FillableCellCustomTable {
            fillableCell.fill(cellModel: cellModel)
        }
        return cell
    }
}
