//
//  WeatherDataSourceCustomCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import UIKit

class WeatherDataSourceCustomCollection: NSObject, DataSourceCustomCollection, UICollectionViewDataSource {
    
    unowned var input: ScreenModelCustomCollection
    
    init(input: ScreenModelCustomCollection) {
        self.input = input
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return input.cellModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard input.cellModel.count > indexPath.row else { return .init() }
        let cellModel = input.cellModel[indexPath.row]
        collectionView.register(typeInString: cellModel.cellIdentifier)
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellModel.cellIdentifier, for: indexPath
        )
        
        cell.backgroundColor = .clear
        if let fillableCell = cell as? FillableCellCustomCollection {
            fillableCell.fill(cellModel: cellModel)
        }
        
        return cell
    }
}
