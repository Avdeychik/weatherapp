//
//  DataSourceCustomCollection.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

protocol DataSourceCustomCollection {
    
    var input: ScreenModelCustomCollection { get }
}
