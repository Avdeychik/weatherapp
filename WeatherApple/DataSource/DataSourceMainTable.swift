//
//  DataSourceMainTable.swift.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import Foundation

protocol DataSourceMainTable {
    
    var input: ScreenModelMainTable { get }
}
