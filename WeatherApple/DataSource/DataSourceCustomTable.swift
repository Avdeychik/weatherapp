//
//  DataSourceCustomTable.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 18.12.21.
//

import Foundation

protocol DataSourceCustomTable {
    
    var input: ScreenModelCustomTable { get }
}
