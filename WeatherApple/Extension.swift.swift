//
//  Extension.swift.swift
//  WeatherApple
//
//  Created by Алексей Авдейчик on 17.12.21.
//

import UIKit

extension UICollectionView {
    func register(typeInString: String) {
        guard let type = NSClassFromString("WeatherApple.\(typeInString)") else { return }
        self.register(type.self, forCellWithReuseIdentifier: typeInString)
    }
}

extension UITableView {
    func register(typeInString: String) {
        guard let type = NSClassFromString("WeatherApple.\(typeInString)") else { return }
        self.register(type.self, forCellReuseIdentifier: typeInString)
    }
}
